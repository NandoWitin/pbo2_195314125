package view1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import model.Mahasiswa;
import model.MasyarakatSekitar;
import model.Penduduk;
import model.UKM;

public class MainFrame extends JFrame implements ActionListener {

    private JMenuBar menuBar;
    private JMenuItem menuItemEdit_tambahMhs;
    private JMenuItem menuItemEdit_tambahMasyarakat;
    private JMenuItem menuItemEdit_tambahUKM;
    private JMenuItem menuItemFile_lihatData;
    private JMenuItem menuItemFile_exit;
    private JMenu menu_File;
    private JMenu menu_Edit;
    private JMenu menu_Help;
    public static UKM ukm = new UKM();
    public static Mahasiswa mhs = new Mahasiswa();
    public static MasyarakatSekitar masya = new MasyarakatSekitar();
    public static Penduduk[] anggota = new Penduduk[5];
    public static int jumlah = 0;

    public MainFrame() {
        initComponents();
    }

    private void initComponents() {
        menuBar = new JMenuBar();

        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");
        menu_Help = new JMenu("Help");

        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        this.setJMenuBar(menuBar);

        menuItemEdit_tambahMhs = new JMenuItem("Tambah Mahasiswa");
        menuItemEdit_tambahMasyarakat = new JMenuItem("Tambah Masyarakat");
        menuItemEdit_tambahUKM = new JMenuItem("Tambah UKM");
        menuItemFile_lihatData = new JMenuItem("Lihat Data");
        menuItemFile_exit = new JMenuItem("Exit");

        menu_Edit.add(menuItemEdit_tambahMhs);
        menu_Edit.add(menuItemEdit_tambahMasyarakat);
        menu_Edit.add(menuItemEdit_tambahUKM);

        menu_File.add(menuItemFile_lihatData);
        menu_File.add(menuItemFile_exit);
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);

        menuItemFile_exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        menuItemEdit_tambahMhs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TambahMahasiswa addMhs = new TambahMahasiswa();
                addMhs.setSize(400, 300);
                addMhs.setVisible(true);
            }
        });

        menuItemEdit_tambahMasyarakat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TambahMasyarakat addMasyarakat = new TambahMasyarakat();
                addMasyarakat.setSize(400, 300);
                addMasyarakat.setVisible(true);
            }
        });

        menuItemEdit_tambahUKM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TambahUKM addUKM = new TambahUKM();
                addUKM.setSize(400, 460);
                addUKM.setVisible(true);
            }
        });

        menuItemFile_lihatData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DaftarMahasiswa mhsTable = new DaftarMahasiswa();
                mhsTable.setSize(300, 400);
                mhsTable.setVisible(true);
            }
        });

    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame main = new MainFrame();
                main.setSize(400, 500);
                main.setVisible(true);
                main.setResizable(false);
                main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
