package model;

public abstract class Penduduk {

    private String nama, tempatTanggalLahir;

    public Penduduk() {

    }

    public Penduduk(String dataNama, String dataTempatTanggalLahir) {
        nama = dataNama;
        tempatTanggalLahir = dataTempatTanggalLahir;
    }

    public void setNama(String dataNama) {
        nama = dataNama;
    }

    public String getNama() {
        return nama;
    }

    public void setTempatTanggalLahir(String dataTempatTanggalLahir) {
        tempatTanggalLahir = dataTempatTanggalLahir;
    }

    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }

    abstract double hitungIuran();
}
