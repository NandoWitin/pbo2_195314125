package model;

public class Mahasiswa extends Penduduk {

    private String nim;

    public Mahasiswa() {
    }

    public Mahasiswa(String dataNis, String dataNama, String dataTempatTanggalLahir) {
        super(dataNama, dataTempatTanggalLahir);
        nim = dataNis;
    }

    public void setNim(String dataNim) {
        nim = dataNim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        return Double.parseDouble(nim) / 10000;
    }
}
