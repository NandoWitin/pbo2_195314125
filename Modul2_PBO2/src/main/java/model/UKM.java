package model;

public class UKM {

    private String namaUnit;
    private Mahasiswa ketua, sekretaris;
    private Penduduk[] anggota;

    public UKM() {

    }

    public UKM(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public String getNamaUnit() {
        return namaUnit;
    }

    public void setKetua(Mahasiswa dataKetua) {
        ketua = dataKetua;
    }

    public Mahasiswa getKetua() {
        return ketua;
    }

    public void setSekretaris(Mahasiswa dataSekretaris) {
        sekretaris = dataSekretaris;
    }

    public Mahasiswa getSekretaris() {
        return sekretaris;
    }

    public void setAnggota(Penduduk[] dataAnggota) {
        anggota = dataAnggota;
    }

    public Penduduk[] getAnggota() {
        return anggota;
    }
}
