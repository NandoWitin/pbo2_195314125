package Modul1_nomor1c;

public class PengujiKelas {

    public static void main(String[] args) {
        Mahasiswa mhs = new Mahasiswa();
        mhs.setNama("Yosefino Mario Fernando Witin");
        mhs.setNim("195314125");

        Handphone hp = new Handphone();
        hp.setMerk("Asus");
        hp.setWarna("Putih");
        hp.setProsesor("Intel Atom Z3560");
        hp.setRam("2 GB");
        hp.setRom("16 GB");
        hp.setKonektivitas("GSM,HSPA,LTE");
        mhs.setHandphone(hp);

        Tablet tab = new Tablet();
        tab.setMerk("Samsung");
        tab.setWarna("Hitam");
        tab.setProsesor("Marvell PXA1088");
        tab.setRam("1,5 GB");
        tab.setRom("8 GB");
        tab.setKonektivitas("Wifi");
        mhs.setTablet(tab);

        Laptop lap = new Laptop();
        lap.setMerk("Lenovo ThinkPad T400");
        lap.setWarna("Hitam");
        lap.setProsesor("Core 2 Duo 2.53 GHz");
        lap.setRam("2 GB");
        lap.setHardisk("160 GB");
        lap.setJumlahUSB(3);
        mhs.setLaptop(lap);

        System.out.println("Nama        : " + mhs.getNama());
        System.out.println("NIM         : " + mhs.getNim());
        System.out.println("");
        System.out.println("Spesifikasi Handphone");
        System.out.println("Merk        : " + mhs.getHandphone().getMerk());
        System.out.println("Warna       : " + mhs.getHandphone().getWarna());
        System.out.println("Prosesor    : " + mhs.getHandphone().getProsesor());
        System.out.println("RAM         : " + mhs.getHandphone().getRam());
        System.out.println("ROM         : " + mhs.getHandphone().getRom());
        System.out.println("Konektivitas: " + mhs.getHandphone().getKonektivitas());
        System.out.println("");
        System.out.println("Spesifikasi Tablet");
        System.out.println("Merk        : " + mhs.getTablet().getMerk());
        System.out.println("Warna       : " + mhs.getTablet().getWarna());
        System.out.println("Prosesor    : " + mhs.getTablet().getProsesor());
        System.out.println("RAM         : " + mhs.getTablet().getRam());
        System.out.println("ROM         : " + mhs.getTablet().getRom());
        System.out.println("Konektivitas: " + mhs.getTablet().getKonektivitas());
        System.out.println("");
        System.out.println("Spesifikasi Laptop");
        System.out.println("Merk            : " + mhs.getLaptop().getMerk());
        System.out.println("Warna           : " + mhs.getLaptop().getWarna());
        System.out.println("Prosesor        : " + mhs.getLaptop().getProsesor());
        System.out.println("RAM             : " + mhs.getLaptop().getRam());
        System.out.println("Hardisk         : " + mhs.getLaptop().getHardisk());
        System.out.println("Jumlah Port USB : " + mhs.getLaptop().getJumlahUSB() + " buah");
    }

}
