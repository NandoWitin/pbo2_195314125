package Modul1_nomor1c;

public class Laptop extends Gadget{

    private String hardisk;
    private int jumlahUSB;

    public String getHardisk() {
        return hardisk;
    }

    public void setHardisk(String hardisk) {
        this.hardisk = hardisk;
    }

    public int getJumlahUSB() {
        return jumlahUSB;
    }

    public void setJumlahUSB(int jumlahUSB) {
        this.jumlahUSB = jumlahUSB;
    }

}
