package Modul1_nomor1b;

public class MasyarakatSekitar extends Penduduk {

    private String nomor;
    
    public MasyarakatSekitar() {
        
    }
    
    public MasyarakatSekitar(String dataNis, String dataNama, String dataTempatTanggalLahir) {
        super(dataNama, dataTempatTanggalLahir);
        nomor = dataNis;
    }
    
    public void setNomor(String dataNomor) {
        nomor = dataNomor;
    }
    
    public String getNomor() {
        return nomor;
    }
    
    @Override
    public double hitungIuran() {
        return Integer.parseInt(nomor) * 100;
    }
}
