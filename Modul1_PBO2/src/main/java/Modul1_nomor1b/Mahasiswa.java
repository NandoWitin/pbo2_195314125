package Modul1_nomor1b;

public class Mahasiswa extends Penduduk {

    private String nim;
    
    public Mahasiswa() {
        
    }
    
    public Mahasiswa(String dataNis, String dataNama, String dataTempatTanggalLahir) {
        super(dataNama, dataTempatTanggalLahir);
        nim = dataNis;
    }
    
    public void setNim(String dataNim) {
        nim = dataNim;
    }
    
    public String getNim() {
        return nim;
    }
    
    @Override
    public double hitungIuran() {
        return Integer.parseInt(nim) / 10000;
    }
    
}
