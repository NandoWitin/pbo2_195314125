package Modul1_nomor1b;

public class Pengujian {

    public static void main(String[] args) {
        UKM unit = new UKM("Himpunan Keluarga Flobamora");
        
        Mahasiswa ketua = new Mahasiswa();
        ketua.setNama("Nando");
        ketua.setNim("195314125");
        ketua.setTempatTanggalLahir("Maumere, 6 Februari 2001");
        unit.setKetua(ketua);
        Mahasiswa sekretaris = new Mahasiswa("195314124", "Nathan", "Yogyakarta, 4 Desember 2001");
        unit.setSekretaris(sekretaris);
        Penduduk[] pend = new Penduduk[5];
        Mahasiswa[] anggotaM = new Mahasiswa[3];
        anggotaM[0] = new Mahasiswa();
        anggotaM[0].setNama("Jhon");
        anggotaM[0].setNim("185314114");
        anggotaM[0].setTempatTanggalLahir("Maumere, 24 September 1998");
        pend[0] = anggotaM[0];
        anggotaM[1] = new Mahasiswa();
        anggotaM[1].setNama("Icho");
        anggotaM[1].setNim("195314130");
        anggotaM[1].setTempatTanggalLahir("Kefa, 9 September 2001");
        pend[1] = anggotaM[1];
        anggotaM[2] = new Mahasiswa();
        anggotaM[2].setNama("Dedit");
        anggotaM[2].setNim("195314146");
        anggotaM[2].setTempatTanggalLahir("Atambua, 24 Juli 2001");
        pend[2] = anggotaM[2];
        
        MasyarakatSekitar[] anggotaMasya = new MasyarakatSekitar[2];
        anggotaMasya[0] = new MasyarakatSekitar();
        anggotaMasya[0].setNama("Richard");
        anggotaMasya[0].setNomor("101");
        anggotaMasya[0].setTempatTanggalLahir("Kupang, 10 November 2001");
        pend[3] = anggotaMasya[0];
        anggotaMasya[1] = new MasyarakatSekitar();
        anggotaMasya[1].setNama("Kevin");
        anggotaMasya[1].setNomor("104");
        anggotaMasya[1].setTempatTanggalLahir("Labuan bajo, 14 Juni 2000");
        pend[4] = anggotaMasya[1];
        
        double total = 0;
        double totalIuranMahasiswa=0, totalIuranMasyarakat=0;
        System.out.println("Daftar Anggota Beserta Iuran Masing-Masing");
        for (int i = 0; i < pend.length; i++) {
            System.out.println("Anggota ke-"+(i+1)+"");
            System.out.println("Nama                    : "+pend[i].getNama());
            System.out.println("Tempat Tanggal Lahir    : "+pend[i].getTempatTanggalLahir());
            if (pend[i] instanceof Mahasiswa) {
                Mahasiswa maha = (Mahasiswa) pend[i];
                System.out.println("NIM                     : "+maha.getNim());
                System.out.println("Iuran                   : "+maha.hitungIuran());
                totalIuranMahasiswa =+ maha.hitungIuran();
            } else if (pend[i] instanceof MasyarakatSekitar) {
                MasyarakatSekitar mas = (MasyarakatSekitar) pend[i];
                System.out.println("Nomor                   : "+mas.getNomor());
                System.out.println("Iuran                   : "+mas.hitungIuran());
                totalIuranMasyarakat =+ mas.hitungIuran();
            }
            System.out.println("");
        }
        total = totalIuranMahasiswa + totalIuranMasyarakat;
        System.out.println("Total Iuran = "+total);
        }
    }


